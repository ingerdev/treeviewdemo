﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeViewDemoLibrary.Datatypes
{
    //more ui-adapted data object.    
    public class DataTagLeaf
    {
        public DataTagLeaf(DataTag dataTag, List<DataTagLeaf> childLeafs)
        {
            DataTag = dataTag;
            if (childLeafs != null)
                ChildLeafs = new List<DataTagLeaf>(childLeafs);
            else
                ChildLeafs = new List<DataTagLeaf>();
        }

        public DataTag DataTag;
        public List<DataTagLeaf> ChildLeafs;
    }
}
