﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeViewDemoLibrary.Datatypes
{
    public class DataTag
    {
        public DataTag(string text,List<string> childTags,string tagId,DataTagEnum tagType)
        {
            Text = text;
            ChildTags = new List<string>(childTags);
            TagId = tagId;
            TagType = tagType;
        }
        public enum DataTagEnum { NoComboboxType,ComboboxType}
        //Tag text 
        public String Text;        
        
        // list of child tags (doesnt include childs of childish tags recursively)
        public List<string> ChildTags;
        
        //unique tag id
        public string TagId;

        public DataTagEnum TagType;
        
        //tag id generate method
        public static string GenerateUniqueId()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}
