﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeViewDemoLibrary.Datatypes
{
    //create test data and organize it as tree
    public class TestDataSource
    {
        public TestDataSource(int itemsCount)
        {
            generate_dataset(itemsCount);
        }
        
        //flat list of DataTags. Hierarcy is written inside DataTags as list of unique ids of childish DataTags.
        //easily can be serializable in/out database
        public List<DataTag> Dataset = new List<DataTag>();

        //generate dataset with items_count DataTags
        private void generate_dataset(int items_count)
        {
            List<string> tagIds = new List<string>();
            List<DataTag.DataTagEnum> tagTypes = new List<DataTag.DataTagEnum>();
            List<DataTag> dataTags = new List<DataTag>();
            Random random = new Random(284);

            for (int k = 0; k < items_count; k++)
            {
                string tagId = DataTag.GenerateUniqueId();
                tagIds.Add(tagId);

                DataTag.DataTagEnum tagType;
                switch (random.Next(2))
                {
                    case 0: tagType = DataTag.DataTagEnum.ComboboxType; break;
                    case 1: tagType = DataTag.DataTagEnum.NoComboboxType; break;
                    default: tagType = DataTag.DataTagEnum.NoComboboxType; break;
                }

                dataTags.Add(new DataTag(generateRandomString(random, random.Next(6) + 2),
                    new List<string>(), tagId, tagType));

            }

            for (int k = 0; k < items_count; k++)
            {
                //10th part of elements should be top in the hierarchy
                if (random.Next(items_count) % 10 == 0)
                    continue;

                //rest of elements randomly add to not yet enumerated nodes
                int parent_index = random.Next(Math.Max(0,items_count-1 - k)) + k + 1;
                //item cannot be parent to itself, also its index cannot exceed the bounds of array
                if ((parent_index == k)||(parent_index>items_count-1))
                    continue;

                dataTags[parent_index].ChildTags.Add(dataTags[k].TagId);
            }
            Dataset = dataTags;
        }
        

        //convert flat dataset list to tree structure (list of lists)
        public List<DataTagLeaf> GenerateTreeByDataset()
        {
            List<DataTagLeaf> root_leafs = new List<DataTagLeaf>();
            Dictionary<string, DataTagLeaf> data_tags_dict = new Dictionary<string, DataTagLeaf>();
            
            //root level, only nodes marked as true are at the root level
            Dictionary<DataTagLeaf, bool> root_marks = new Dictionary<DataTagLeaf, bool>();
            foreach(var data_tag in Dataset)
            {
                //create leaf object for every dataTag object and put it in dict by TagId key
                data_tags_dict[data_tag.TagId] = new DataTagLeaf(data_tag,null);
                
                //init every node as root.
                root_marks[data_tags_dict[data_tag.TagId]] = true;
            }


            //put every child to his hierarcy place
            foreach(DataTagLeaf leaf in data_tags_dict.Values)
            {
                //find children of this node and add it to current leaf list
                foreach(string child_id in leaf.DataTag.ChildTags)
                {                    
                    leaf.ChildLeafs.Add(data_tags_dict[child_id]);
                    //as node is children of someone, drop its root mark
                    root_marks[data_tags_dict[child_id]] = false;
                }
            }

            //fill root level (leafs with no parents)
            foreach(var possible_root_leaf in root_marks.Keys)
            {
                if (root_marks[possible_root_leaf])
                    root_leafs.Add(possible_root_leaf);
            }

            //return list of root leafs. each leaf keep list of its children.
            return root_leafs;
        }

        #region utility methods
        private string generateRandomString(Random random,int string_length)
        {
            var chars = "abcdefghijklmnopqrstuvwxyz123456789".ToArray();
            string pw = Enumerable.Range(0, string_length)
                                  .Aggregate(
                                      new StringBuilder(),
                                      (sb, n) => sb.Append((chars[random.Next(chars.Length)])),
                                      sb => sb.ToString());
            return pw;
        }
        #endregion
    }
}
