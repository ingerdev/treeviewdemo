﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using TreeViewDemoLibrary.Datatypes;

namespace TreeViewDemoLibrary.Converters
{
    public class DataTagTypeToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (value == null)
                return Visibility.Collapsed;

            DataTag.DataTagEnum data_tag_type = (DataTag.DataTagEnum)value;
            switch (data_tag_type)
            {
                case DataTag.DataTagEnum.ComboboxType: return Visibility.Visible;
                case DataTag.DataTagEnum.NoComboboxType: return Visibility.Collapsed;
                default: return Visibility.Collapsed;
            }
           
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
