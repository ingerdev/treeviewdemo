﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreeViewDemoLibrary.Datatypes;
using static TreeViewDemoLibrary.Datatypes.TestDataSource;

namespace TreeViewDemoLibrary.ViewModels
{
    public class DataTagLeafViewModel:INotifyPropertyChanging,INotifyPropertyChanged
    {
        private DataTagLeaf _data_tag_leaf;
        public DataTagLeafViewModel(DataTagLeaf data_tag_leaf, List<DataTagLeafViewModel> children_viewmodels )
        {
            _data_tag_leaf = data_tag_leaf;
            Children = new ObservableCollection<DataTagLeafViewModel>(children_viewmodels);
            
        }

        #region data-related properties
        
        //DataTag text value  
        public string Text
        {
            get { return _data_tag_leaf.DataTag.Text; }
            set
            {
                NotifyPropertyChanging("Text");
                _data_tag_leaf.DataTag.Text = value;
                NotifyPropertyChanged("Text");
            }
        }

        //type of DataTag (depend of this type we will use one of another DataTemplate
        public DataTag.DataTagEnum DataTagType
        {
            get { return _data_tag_leaf.DataTag.TagType; }
            private set { }
        }

        public ObservableCollection<DataTagLeafViewModel> Children { get; set; }
        #endregion

        #region ui-related properties
        //public IsSelected
        #endregion

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;       
        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion

    }
}
