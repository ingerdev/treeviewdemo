﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreeViewDemoLibrary.Datatypes;
using static TreeViewDemoLibrary.Datatypes.TestDataSource;

namespace TreeViewDemoLibrary.ViewModels
{
    class CustomTreeViewViewModel : INotifyPropertyChanging, INotifyPropertyChanged
    {
        //for internal usage
        public ObservableCollection<DataTagLeafViewModel> Items { get; set; }
        

        public CustomTreeViewViewModel()
        {
            Items = new ObservableCollection<DataTagLeafViewModel>();
        }

        //called from CustomTreeView when ItemsSource changed
        public void updateItems(List<DataTagLeaf> dataTags)
        {
            Items.Clear();
            //we dont use data virtualization so will create
            //full hierarcy of DataTagLeafViewModel classes.            
            foreach (var item in populateDataTagViewModels(dataTags))
                Items.Add(item);
        }

        //from DataTagLeaf tree create DataTagLeafViewModel tree
        private List<DataTagLeafViewModel> populateDataTagViewModels(List<DataTagLeaf> dataTagsLeafs)
        {
            List<DataTagLeafViewModel> models = new List<DataTagLeafViewModel>();
            foreach(var leaf in dataTagsLeafs)
            {
                models.Add(recursivelyCreateDataTagLeafsViewModels(leaf));
            }
            return models;
        }

        //recursively create tree of DataTagLeaf view models
        private DataTagLeafViewModel recursivelyCreateDataTagLeafsViewModels(DataTagLeaf leaf)
        {
            List<DataTagLeafViewModel> children_viewmodels = new List<DataTagLeafViewModel>();
            foreach (var child_leaf in leaf.ChildLeafs)
            {
                children_viewmodels.Add(recursivelyCreateDataTagLeafsViewModels(child_leaf));
            }

            DataTagLeafViewModel leaf_viewmodel = new DataTagLeafViewModel(leaf, children_viewmodels);
            return leaf_viewmodel;
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }
}
