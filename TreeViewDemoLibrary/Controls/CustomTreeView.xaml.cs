﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TreeViewDemoLibrary.ViewModels;
using TreeViewDemoLibrary.Datatypes;

namespace TreeViewDemoLibrary.Controls
{
    /// <summary>
    /// Interaction logic for CustomTreeView.xaml
    /// </summary>
    public partial class CustomTreeView : UserControl
    {
        public static readonly DependencyProperty ItemsSourceDependencyProperty =
DependencyProperty.Register("ItemsSource", typeof(List<DataTagLeaf>), typeof(CustomTreeView), new PropertyMetadata(null, new PropertyChangedCallback(ItemsSourcePropertyChanged)));

        public List<DataTagLeaf> ItemsSource
        {
            get { return (List<DataTagLeaf>)GetValue(ItemsSourceDependencyProperty); }
            set { SetValue(ItemsSourceDependencyProperty, value); }
        }

        private static void ItemsSourcePropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {

            CustomTreeView customTreeView = (CustomTreeView)o;
            if (customTreeView == null)
                return;
            customTreeView.updateItemsSource((List<DataTagLeaf>)e.NewValue);

        }


        public CustomTreeView()
        {
            InitializeComponent();
            MainGrid.DataContext = new CustomTreeViewViewModel();
        }

        private void updateItemsSource(List<DataTagLeaf> dataTagLeafs)
        {
            var vm = (CustomTreeViewViewModel)MainGrid.DataContext;
            vm.updateItems(dataTagLeafs);
        }
    }
}
